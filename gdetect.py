import cv2
import numpy as np
import math

img = cv2.imread(input())
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

size = np.size(gray)
skeleton = np.zeros(gray.shape, np.uint8)
 
ret, gray = cv2.threshold(gray, 127, 255, 0)
element = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))

zeros = None
 
while zeros != size:
	eroded = cv2.erode(gray, element)
	temp = cv2.dilate(eroded, element)
	temp = cv2.subtract(gray, temp)
	skeleton = cv2.bitwise_or(skeleton, temp)
	gray = eroded.copy()
 
	zeros = size - cv2.countNonZero(gray)

cv2.imwrite('out_skel.png', skeleton)

lines_img = np.copy(img) * 0
overlay = np.copy(img) * 0

RHO = 1
THETA = np.pi / 180
THRESHOLD = 5
MIN_LINE_LEN = 3
MAX_LINE_SPACING = 3
line_image = np.copy(img) * 0
line_image_narrow = np.copy(img) * 0
point_overlay = np.copy(img) * 0

lines = cv2.HoughLinesP(skeleton, RHO, THETA, THRESHOLD, np.array([]), MIN_LINE_LEN, MAX_LINE_SPACING)

connections = [[] for i in range(len(lines))]

IN_RANGE_DIST = 20

def points_match(x1, y1, x2, y2):
	return abs(x2 - x1) <= IN_RANGE_DIST and abs(y2 - y1) <= IN_RANGE_DIST
	
def ccw(x1, y1, x2, y2, x3, y3):
    return (y3 - y1) * (x2 - x1) > (y2 -y1) * (x3 - x1)
	
def intersect(x1, y1, x2, y2, x3, y3, x4, y4):
    return ccw(x1, y1, x3, y3, x4, y4) != ccw(x2, y2, x3, y3, x4, y4) and ccw(x1, y1, x2, y2, x3, y3) != ccw(x1, y1, x2, y2, x4, y4)

def get_dist_sqr(x1, y1, x2, y2):
	return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)

def get_dist_to_seg_sqr(px, py, x1, y1, x2, y2):
	dist_sqr = get_dist_sqr(x1, y1, x2, y2)
	
	if dist_sqr == 0:
		return get_dist_sqr(px, py, x1, y1)
		
	t = ((px - x1) * (x2 - x1) + (py - y1) * (y2 - y1)) / dist_sqr
	t = max(0, min(1, t))
	
	return get_dist_sqr(px, py, x1 + t * (x2 - x1), y1 + t * (y2 - y1))

def endpoint_near(px, py, x1, y1, x2, y2):
	res = math.sqrt(get_dist_to_seg_sqr(px, py, x1, y1, x2, y2))

	return math.sqrt(get_dist_to_seg_sqr(px, py, x1, y1, x2, y2)) <= IN_RANGE_DIST

def near(x1, y1, x2, y2, x3, y3, x4, y4):
	return endpoint_near(x1, y1, x3, y3, x4, y4) or \
		endpoint_near(x2, y2, x3, y3, x4, y4) or \
		endpoint_near(x3, y3, x1, y1, x2, y2) or \
		endpoint_near(x4, y4, x1, y1, x2, y2)
	
IN_RANGE_ANGLE = math.pi / 8
	
def sim_angles(x1, y1, x2, y2, x3, y3, x4, y4):
	return abs(math.atan2(y2 - y1, x2 - x1) - math.atan2(y4 - y3, x4 - x3)) < IN_RANGE_ANGLE

for i in range(len(lines)):
	for x1, y1, x2, y2 in lines[i]:
		for j in range(i):
			for x1p, y1p, x2p, y2p in lines[j]:			
				if (points_match(x1, y1, x1p, y1p) or points_match(x2, y2, x2p, y2p) or \
					points_match(x1, y1, x2p, y2p) or points_match(x2, y2, x1p, y1p) or \
					intersect(x1, y1, x2, y2, x1p, y1p, x2p, y2p) or \
					near(x1, y1, x2, y2, x1p, y1p, x2p, y2p)) and \
					sim_angles(x1, y1, x2, y2, x1p, y1p, x2p, y2p):
										
					connections[i].append(j)
					connections[j].append(i)
					
matched = [False] * len(lines)

matches = []
match = []

idx = len(lines) - 1

def find_match(idx):
	matched[idx] = True
	match.append(idx)

	for connection in connections[idx]:	
		if not connection in match:
			find_match(connection)

while idx > 0:
	find_match(idx)
	
	if len(match) > 0:
		matches.append(match)
	
		print(match)
		match = []
	
	while matched[idx] and idx > 0:
		idx -= 1
	
candidate_edges = []
endpoints = []

for match in matches:
	min_x = 10000000
	max_x = -1
	
	min_y = 0
	max_y = 0

	for idx in match:
		for x1, y1, x2, y2 in lines[idx]:		
			if x1 < min_x:
				min_x = x1
				min_y = y1
				
			if x2 < min_x:
				min_x = x2
				min_y = y2
				
			if x1 > max_x:
				max_x = x1
				max_y = y1
				
			if x2 > max_x:
				max_x = x2
				max_y = y2
				
	candidate_edges.append([min_x, min_y, max_x, max_y])
	endpoints.extend([[min_x, min_y], [max_x, max_y]])
	
	print(min_x, min_y, max_x, max_y)

vertex_partitions = []
vertices = []

IN_RANGE_VERT_DIST = 100
HALF_IN_RANGE_VERT_DIST = IN_RANGE_VERT_DIST / 2

for endpoint in endpoints:
	ex, ey = endpoint

	inserted = False

	for p in vertex_partitions:
		if len(p) == 1:		
			fx, fy = p[0]
			
			if abs(fx - ex) < IN_RANGE_VERT_DIST and abs(fy - ey) < IN_RANGE_VERT_DIST:
				p.append(endpoint)
				inserted = True
		else:
			mid_x = sum([e[0] for e in p]) / len(p)
			mid_y = sum([e[1] for e in p]) / len(p)
			
			if abs(ex - mid_x) < HALF_IN_RANGE_VERT_DIST and abs(ey - mid_y) < HALF_IN_RANGE_VERT_DIST:
				p.append(endpoint)
				inserted = True
				
		if inserted:
			break
		
	if not inserted:
		vertex_partitions.append([endpoint])

for p in vertex_partitions:
	vertices.append([sum([e[0] for e in p]) // len(p), sum([e[1] for e in p]) // len(p)])
	
edges = []
	
print(len(candidate_edges))

for edge in candidate_edges:
	x1, y1, x2, y2 = edge
	
	count = 0
	
	for v in vertices:
		vx, vy = v
		
		if math.sqrt((x1 - vx) * (x1 - vx) + (y1 - vy) * (y1 - vy)) >= HALF_IN_RANGE_VERT_DIST or \
			math.sqrt((x2 - vx) * (x2 - vx) + (y2 - vy) * (y2 - vy)) >= HALF_IN_RANGE_VERT_DIST:
			
			count += 1
			
	if count == len(vertices):
		edges.append(edge)
	
edge_overlay = np.copy(img) * 0

for edge in edges:
	x1, y1, x2, y2 = edge
	
	cv2.line(edge_overlay, (x1, y1), (x2, y2), (255, 0, 0), 5)
	
vertex_overlay = np.copy(img) * 0
	
for v in vertices:
	cv2.circle(vertex_overlay, (v[0], v[1]), 20, (0, 255, 0), -1)
	
img_cpy = np.copy(img)
h, _, _ = img.shape

cv2.putText(img_cpy, 'edges: %s vertices: %s' % (len(edges), len(vertices)), (10, h - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1);
	
cv2.imwrite('out_graph.png', cv2.addWeighted(cv2.addWeighted(vertex_overlay, 1, edge_overlay, 1, 0), 0.8, img_cpy, 1, 0))
	
for line in lines:
	for x1, y1, x2, y2 in line:		
		cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 5)
		cv2.line(line_image_narrow, (x1, y1), (x2, y2), (255, 255, 255), 1)
		
		cv2.circle(point_overlay, (x1, y1), 3, (0, 255, 0), -1)
		cv2.circle(point_overlay, (x2, y2), 3, (0, 0, 255), -1)

lines_edges = cv2.addWeighted(img, 0.8, line_image, 1, 0)
final = cv2.addWeighted(lines_edges, 0.8, point_overlay, 1, 0)

for i in range(len(lines)):
	for x1, y1, x2, y2 in lines[i]:
		font                   = cv2.FONT_HERSHEY_SIMPLEX
		bottomLeftCornerOfText = ((x1 + x2) // 2, (y1 + y2) // 2)
		fontScale              = 0.25
		fontColor              = (255,255,255)
		lineType               = 1

		cv2.putText(final, str(i),
			bottomLeftCornerOfText, 
			font, 
			fontScale,
			fontColor,
			lineType)

cv2.imwrite('out_lines.png', cv2.addWeighted(point_overlay, 1, line_image_narrow, 1, 0))
cv2.imwrite('out_test.png', final)